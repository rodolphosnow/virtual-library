Rails.application.routes.draw do
  root to: 'books#index'
  devise_for :librarians, path: 'librarian', path_names: {sign_in: 'login',   sign_out: 'logout', password: 'senha'}
  devise_for :users, path: 'user', path_names: {sign_in: 'login',   sign_out: 'logout', password: 'senha'}
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  resources :books do
    get 'search', action: 'search', on: :collection
  end

  resources :users, only: [] do
    get 'make-favorite/:book_id', action: 'make_favorite', on: :collection, as: :make_a_book_favorite
    get 'favorites', action: 'favorites', on: :collection
  end

  namespace :librarians do
    resources :dashboard
    resources :books do
      get 'search', action: 'search', on: :collection
    end
  end

end
