class Librarians::BooksController < LibrariansController

  before_action :book, only: [ :edit, :update]
  
  def book
    @book = Book.find(params[:id])
  end

  def index
    @books = Book.all.page(params[:page]).per(5)
  end

  def new
    @authors = Author.all
    @book = Book.new
  end

  def create
    @book = Book.new(book_params)
    if @book.save
      flash[:notice] = "Book successfuly created!"
      redirect_to [:librarians, :books]
    else
      flash[:notice] = "There was a problem creating the book, please try again"
      render 'new'
    end
  end

  def edit
    @authors = Author.all
    @book = Book.find(params[:id])
  end

  def update
    if @book.update(book_params)
      flash[:notice] = "Book successfuly updated!"
      redirect_to [:librarians, :books]
    else
      flash[:notice] = "There was a problem updating the book, please try again"
      render 'edit'
    end
  end

  def book_params
    params.require(:book).permit(:title, :description, :author_id, :image)
  end
end