class BooksController < ApplicationController
  layout 'users'
  protect_from_forgery except: :search

  def index
    @books = Book.order(:title).page(params[:page]).per(12)
    @authors = Author.all
  end

  def search
    term_criteria = params[:search][:term].present? ? "%#{params[:search][:term]}%" : nil
    author_criteria = params[:search][:author]
    order = params[:search][:order] || "asc"
    if author_criteria.present? && term_criteria.present?
      @books = Book.where('author_id = ? AND (title ilike ? or description ilike ?)',author_criteria, term_criteria, term_criteria).order(title: order).page(params[:page]).per(12)
    elsif term_criteria.present?
      @books = Book.where('title ilike ? or description ilike ?', term_criteria, term_criteria).order(title: order).page(params[:page]).per(12)
    else
      @books = Book.where(author_id: author_criteria).order(title: order).page(params[:page]).per(12)
    end
    render 'search', format: :js
  end
  
end
