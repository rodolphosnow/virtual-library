class UsersController < ApplicationController
  layout 'users'
  before_action :authenticate_user!
  
  def make_favorite
    book_id = params[:book_id]
    current_user.favorite_books.build(book_id: book_id)
    if current_user.save
      @user = current_user
      @favorites = @user.favorite_books.collect{|x| x.book}
      redirect_to [:favorites, :users]
    else
      @errors = "Não é possível favoritar o mesmo livro mais de uma vez."
      render 'error'
    end
  end

  def favorites
    books_ids = current_user.favorite_books.pluck(:book_id)
    @favorites = Book.where(id: books_ids).page(params[:page]).per(12)
  end

end