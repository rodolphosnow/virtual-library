class LibrariansController < ApplicationController
  layout 'librarians'
  before_action :authenticate_librarian!
end
