class FavoriteBook < ApplicationRecord
  belongs_to :user
  belongs_to :book

  validate :book_user_combination_uniqueness


  def book_user_combination_uniqueness
    if FavoriteBook.where(book_id: book_id, user_id: user_id).present?
      errors.add(:book_id, "Book and user combination already used")
    end
  end
end
