class Author < ApplicationRecord
  validates :name, presence: true, length: { in: 2..40  }

  def to_s
    name
  end
end
