class Book < ApplicationRecord
  belongs_to :author
  validates :title, presence: true
  validates :author_id, presence: true
  validates :description, presence: true
  validates :image, presence: true
  has_many :favorite_books
  has_many :users, through: :favorite_books
end
