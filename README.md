
# README


# Framework versions
- Ruby 3.0.1
- Rails 6
- bundler 2.2.31
- Postgresql 12
- Boostrap 5

# Included gems
- **Simple Form** https://github.com/heartcombo/simple_form 
"Simple Form aims to be as flexible as possible while helping you with powerful components to create your forms."
- **Devise** https://github.com/heartcombo/devise
"Devise is a flexible authentication solution for Rails based on Warden. It:"
- **Rspec** https://github.com/rspec/rspec-rails
"Rspec test framework for Ruby on Rails"

- **Shoulda-matchers** "https://github.com/thoughtbot/shoulda-matchers
"Shoulda Matchers provides RSpec- and Minitest-compatible one-liners to test common Rails functionality that, if written by hand, would be much longer, more complex, and error-prone.:"
- **Kaminari** https://github.com/kaminari/kaminari
"A Scope & Engine based, clean, powerful, customizable and sophisticated paginator for modern web app frameworks and ORMs"
- **Slim** http://slim-lang.com/
"Slim is a Ruby template language whose goal is reduce the syntax to the essential parts without becoming cryptic."

# Configuration
- To start the project, with rails 6, Ruby 3.0.1 and bundler 2.2.31 installed, in the root folder of the project is necessary to run in the terminal ``  
 bundle install ``
- After completed to install the dependencies, before starting the project is necessary to follow the **Database Creation** steps

# Database creation
- Using the terminal, in the root folder of the project run in the given order
- `rake db:create`
- `rake db:migrate`
- `rake db:seed`

# How to run the test suite
- Using the terminal, in the root folder of the project run
- `bundle exec rspec spec/`
