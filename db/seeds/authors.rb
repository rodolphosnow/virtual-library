puts "Creating Authors!"
authors = [
  {id: 1, name: "J. R. R. Tolkien"},
  {id: 2, name: "G. R. R. Martin"},
  {id: 3, name: "Leonel Caldela"}
]

authors.each{ |author| Author.create(id: author[:id], name: author[:name]) }