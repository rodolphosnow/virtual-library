puts "Creating Books!"
books = [
  {
    author_id: 1,
    title: "The Lord of the Rings: The Fellowship of the Ring", 
    description: "The dark, fearsome Ringwraiths ....",
    image: "https://m.media-amazon.com/images/I/51waks01PAL.jpg"
  },
  {
    author_id: 1,
    title: "The Hobbit ", 
    description: "The Hobbit, or There and Back Again é um livro infantojuvenil de alta fantasia escrito pelo filólogo e professor britânico J. R. R. Tolkien", 
    image: "https://upload.wikimedia.org/wikipedia/pt/thumb/7/72/The_Hobbit_Cover.JPG/230px-The_Hobbit_Cover.JPG"
  },
  {
    author_id: 2,
    title: "A Guerra dos Tronos", 
    description: "A Game of Thrones é o primeiro livro da série de fantasia épica As Crônicas de Gelo e Fogo, escrita pelo norte-americano George R. R. Martin e publicada pela editora Bantam Spectra", 
    image: "https://upload.wikimedia.org/wikipedia/pt/thumb/c/cc/AGameofThrones1996.jpg/220px-AGameofThrones1996.jpg"
  },
  {
    author_id: 2,
    title: "A Clash of kings", 
    description: "A Game of Thrones é a série de fantasia épica, escrita pelo norte-americano George R. R. Martin e publicada pela editora Bantam Spectra", 
    image: "https://upload.wikimedia.org/wikipedia/pt/thumb/c/cc/AGameofThrones1996.jpg/220px-AGameofThrones1996.jpg"
  },
  {
    author_id: 2,
    title: "A Storm of Swords", 
    description: "A Game of Thrones é a série de fantasia épica, escrita pelo norte-americano George R. R. Martin e publicada pela editora Bantam Spectra", 
    image: "https://upload.wikimedia.org/wikipedia/pt/thumb/c/cc/AGameofThrones1996.jpg/220px-AGameofThrones1996.jpg"
  },
  {
    author_id: 2,
    title: "A Feast for Crows", 
    description: "A Game of Thrones é a série de fantasia épica, escrita pelo norte-americano George R. R. Martin e publicada pela editora Bantam Spectra", 
    image: "https://upload.wikimedia.org/wikipedia/pt/thumb/c/cc/AGameofThrones1996.jpg/220px-AGameofThrones1996.jpg"
  },
  {
    author_id: 2,
    title: "A Dance With Dragons", 
    description: "A Game of Thrones é a série de fantasia épica, escrita pelo norte-americano George R. R. Martin e publicada pela editora Bantam Spectra", 
    image: "https://upload.wikimedia.org/wikipedia/pt/thumb/c/cc/AGameofThrones1996.jpg/220px-AGameofThrones1996.jpg"
  },
  {
    author_id: 2,
    title: "The Winds of Winter", 
    description: "A Game of Thrones é a série de fantasia épica, escrita pelo norte-americano George R. R. Martin e publicada pela editora Bantam Spectra", 
    image: "https://upload.wikimedia.org/wikipedia/pt/thumb/c/cc/AGameofThrones1996.jpg/220px-AGameofThrones1996.jpg"
  },
  {
    author_id: 2,
    title: "A Dream of Spring", 
    description: "A Game of Thrones é a série de fantasia épica, escrita pelo norte-americano George R. R. Martin e publicada pela editora Bantam Spectra", 
    image: "https://upload.wikimedia.org/wikipedia/pt/thumb/c/cc/AGameofThrones1996.jpg/220px-AGameofThrones1996.jpg"
  }


]

books.each do |book|
  Book.create(
    author_id: book[:author_id],
    title: book[:title],
    description: book[:description],
    image: book[:image]
  )
end