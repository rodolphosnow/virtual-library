puts "Creating Users!"
users = [
  {id: 1, name: "Jonas", email: "teste1@gmail.com", password: "123456"},
  {id: 2, name: "Bruna", email: "teste2@gmail.com", password: "123456"},
  {id: 3, name: "Elias", email: "teste3@gmail.com", password: "123456"},
  {id: 4, name: "Joaquim", email: "teste3@gmail.com", password: "123456"}
]

users.each do |user| 
  available_books_ids = Book.all.pluck(:id)
  u = User.create(
    id: user[:id], 
    name: user[:name], 
    email: user[:email], 
    password: user[:password]
  )
  u.favorite_books.build(book_id: available_books_ids.sample)
  u.save
end