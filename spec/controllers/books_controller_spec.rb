require "rails_helper"

RSpec.describe BooksController, type: :controller do
  before do
    author_objs = [
      {id: 1, name: "J. R. R. Tolkien"},
      {id: 2, name: "G. R. R. Martin"},
      {id: 3, name: "Leonel Caldela"}
    ]
    author_objs.each{ |author_obj| Author.create(id: author_obj[:id], name: author_obj[:name]) }
    authors = Author.all
    20.times do |index|
      Book.create(
        author_id: authors.sample.id,
        title: "Teste #{index}",
        description: "Description #{index}",
        image: "https://m.media-amazon.com/images/I/51waks01PAL.jpg"
      )
    end
  end
  describe "Books requests" do
    it "GET index returns the books index" do
      get :index
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
    end

    it "GET search returns the searched books" do
      get :search, format: 'js', params: {search: {term: "Teste 1"}}
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:search)
    end
  end

end