require "rails_helper"

RSpec.describe Librarians::DashboardController, type: :controller do
  describe "GET index with signed user" do
    before do
      @librarian = Librarian.create(
        name: "Jonas", 
        email: "jonasteste@gmail.com", 
        password: "12345678"
      )
      sign_in(@librarian)
    end

    it "returns a 200" do
      get :index
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
    end
  end

  describe "GET index without a signed user" do
    it "returns 302" do
      get :index
      expect(response).to have_http_status(:found)
      expect(response).to_not render_template(:index)
    end
  end

end