require "rails_helper"

RSpec.describe Librarians::BooksController, type: :controller do
  describe "Requests with signed librarian" do
    before do
      @librarian = Librarian.create(
        name: "Jonas", 
        email: "jonasteste@gmail.com", 
        password: "12345678"
      )
      sign_in(@librarian)
    end

    it "GET index returns a 200" do
      get :index
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
    end

    it "GET new returns 200" do
      get :new
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:new)
    end

    it "POST create returns 200 with valid attributes" do 
      author = Author.create(name: "Bastion")
      book = Book.new(
        title: "whatever book", 
        description: "A simple book...",
        image: "https://m.media-amazon.com/images/I/51waks01PAL.jpg",
        author_id: author.id 
      )
      post :create, params: { 
        book: { 
          title: book.title, 
          description: book.description,
          image: book.image,
          author_id: book.author_id 
          } 
        }
      created_book = Book.last
      expect(book.title).to be_eql(created_book.title)
      expect(book.description).to be_eql(created_book.description)
      expect(book.image).to be_eql(created_book.image)
      expect(book.author_id).to be_eql(created_book.author_id)
      expect(response).to have_http_status(:found)
    end

    it "POST create returns 200 with invalid attributes" do 
      author = Author.create(name: "Bastion")
      book = Book.new(
        title: "whatever book", 
        description: "A simple book...",
        image: "https://m.media-amazon.com/images/I/51waks01PAL.jpg",
        author_id: author.id 
      )
      post :create, params: { 
        book: { 
          title: nil, 
          description: book.description,
          image: book.image,
          author_id: book.author_id 
          } 
        }
      created_book = Book.last
      expect(created_book).to be_eql(nil)
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:new)
    end

    it "GET edit returns 200" do
      author = Author.create(name: "Bastion")
      book = Book.create(
        title: "whatever book", 
        description: "A simple book...",
        image: "https://m.media-amazon.com/images/I/51waks01PAL.jpg",
        author_id: author.id 
      )
      get :edit, params: {id: book.id}
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:edit)
    end

    it "PUT update persists the changes with valid attributes" do 
      author = Author.create(name: "Bastion")
      book = Book.create(
        title: "whatever book", 
        description: "A simple book...",
        image: "https://m.media-amazon.com/images/I/51waks01PAL.jpg",
        author_id: author.id 
      )
      put :update, params: { 
        id: book.id,
        book: { 
          title: "New title", 
          description: "New description",
          image: book.image,
          author_id: book.author_id 
          } 
        }
      updated_book = Book.last
      expect(updated_book.title).to be_eql("New title")
      expect(updated_book.description).to be_eql("New description")
      expect(book.image).to be_eql(updated_book.image)
      expect(book.author_id).to be_eql(updated_book.author_id)
      expect(response).to have_http_status(:found)
    end
  end

  describe "Requests without a signed librarian" do
    it "GET index returns 302" do
      get :index
      expect(response).to have_http_status(:found)
      expect(response).to_not render_template(:index)
    end

    it "GET new returns 302" do
      get :new
      expect(response).to have_http_status(:found)
      expect(response).to_not render_template(:new)
    end

    it "POST create returns 302" do 
      author = Author.create(name: "Bastion")
      book = Book.new(
        title: "whatever book", 
        description: "A simple book...",
        image: "https://m.media-amazon.com/images/I/51waks01PAL.jpg",
        author_id: author.id 
      )
      post :create, params: { 
        book: { 
          title: book.title, 
          description: book.description,
          image: book.image,
          author_id: book.author_id 
          } 
        }
      created_book = Book.last
      expect(created_book).to be_eql(nil)
      expect(response).to have_http_status(:found)
    end

    it "GET edit returns 302" do
      author = Author.create(name: "Bastion")
      book = Book.create(
        title: "whatever book", 
        description: "A simple book...",
        image: "https://m.media-amazon.com/images/I/51waks01PAL.jpg",
        author_id: author.id 
      )
      get :edit, params: {id: book.id}
      expect(response).to have_http_status(:found)
    end
  end

end