require "rails_helper"

RSpec.describe UsersController, type: :controller do
  before do
    author_objs = [
      {id: 1, name: "J. R. R. Tolkien"},
      {id: 2, name: "G. R. R. Martin"},
      {id: 3, name: "Leonel Caldela"}
    ]
    author_objs.each{ |author_obj| Author.create(id: author_obj[:id], name: author_obj[:name]) }
    authors = Author.all
    20.times do |index|
      Book.create(
        author_id: authors.sample.id,
        title: "Teste #{index}",
        description: "Description #{index}",
        image: "https://m.media-amazon.com/images/I/51waks01PAL.jpg"
      )
    end
    @user = User.create(
      name: "Jonas", 
      email: "jonasteste@gmail.com", 
      password: "12345678"
    )
    sign_in(@user)
  end

  describe "Make a book favorite requests" do
    it "GET make favorite when pick a valid favorite redirects to the favorite books" do
      favorite_book = Book.first
      get :make_favorite, params: {book_id: favorite_book.id}
      favorites_count = @user.favorite_books.count
      expect(favorites_count).to be_eql(1)
      expect(response).to redirect_to([:favorites, :users])
    end

    it "GET make favorite when pick a invalid favorite render a error message" do
      favorite_book = Book.first
      get :make_favorite, params: {book_id: favorite_book.id}
      get :make_favorite, params: {book_id: favorite_book.id}
      favorites_count = @user.favorite_books.count
      expect(favorites_count).to be_eql(1)
      expect(response).to render_template(:error)
    end
  end

  describe "Get favorites returns user favorites" do
    it "returns user favorites books" do
      get :favorites
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:favorites)
    end
  end
end