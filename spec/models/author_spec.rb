require 'rails_helper'

RSpec.describe Author, type: :model do
  it "is valid with valid attributes" do
    author = Author.new(name: "J. R. R. Tolkien")
    expect(author).to be_valid
  end

  it "is not valid without a name" do
    author = Author.new(name: nil)
    author.valid?
    expect(author.errors.messages.keys.include?(:name))
    expect(author).to_not be_valid
  end

  it "is not valid with name length bellow 2" do
    author = Author.new(name: "J")
    author.valid?
    expect(author.errors.messages.keys.include?(:name))
    expect(author).to_not be_valid
  end

  it "is not valid with name length above 60" do
    author = Author.new(name: "abcdefghijklmnopkrstuvxzabcdefghijklmnopkrstuvxz1234567890123")
    author.valid?
    expect(author.errors.messages.keys.include?(:name))
    expect(author).to_not be_valid
  end

  it "has to respond to to_s with its name" do
    author = Author.new(name: "Lucinda")
    expect(author.to_s).to be_eql(author.name)
  end
end
