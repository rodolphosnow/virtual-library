require 'rails_helper'

RSpec.describe User, type: :model do

  it "has many favorite_books" do
    should have_many(:favorite_books)
  end

  it "is valid with valid attributes" do
    user = User.new(name: "John", email: "johnteste@gmail.com", password: "teste123")
    expect(user).to be_valid
  end

  it "is not valid without a name" do
    user = User.new(name: nil, email: "johnteste@gmail.com", password: "teste123")
    user.valid?
    expect(user.errors.messages.keys.include?(:name))
    expect(user).to_not be_valid
  end

  it "is not valid without a password" do
    user = User.new(name: "John", email: "johnteste@gmail.com", password: nil)
    user.valid?
    expect(user.errors.messages.keys.include?(:password))
    expect(user).to_not be_valid
  end

  it "is not valid without a email" do
    user = User.new(name: "John", email: nil, password: "teste123")
    user.valid?
    expect(user.errors.messages.keys.include?(:email))
    expect(user).to_not be_valid
  end

  it "is not valid with name length bellow 2" do
    user = User.new(name: "J", email: "johnteste@gmail.com", password: "teste123")
    user.valid?
    expect(user.errors.messages.keys.include?(:name))
    expect(user).to_not be_valid
  end

  it "is not valid with name length above 60" do
    user = User.new(name: "abcdefghijklmnopkrstuvxzabcdefghijklmnopkrstuvxz1234567890123", email: "johnteste@gmail.com", password: "teste123")
    user.valid?
    expect(user.errors.messages.keys.include?(:name))
    expect(user).to_not be_valid
  end

end
