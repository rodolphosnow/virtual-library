require 'rails_helper'

RSpec.describe Book, type: :model do
  before(:each) do
    @author = Author.create(name: "J. R. R. Tolkien")
  end

  it "has many favorite_books" do
    should have_many(:favorite_books)
  end

  it "has many users through books" do
    should have_many(:users)
  end

  it "is valid with valid attributes" do
    book = Book.new(
      author: @author, 
      title: "The Lord of the Rings: The Fellowship of the Ring", 
      description: "The dark, fearsome Ringwraiths ....",
      image: "https://m.media-amazon.com/images/I/51waks01PAL.jpg"
    )
    expect(book).to be_valid
  end

  it "is not valid without an author" do
    book = Book.new(
      author: nil, 
      title: "The Lord of the Rings: The Fellowship of the Ring", 
      description: "The dark, fearsome Ringwraiths ....",
      image: "https://m.media-amazon.com/images/I/51waks01PAL.jpg"
    )
    expect(book.errors.messages.keys.include?(:author))
    expect(book).to_not be_valid
  end

  it "is not valid without a title" do
    book = Book.new(
      author: @author, 
      title: nil, 
      description: "The dark, fearsome Ringwraiths ....",
      image: "https://m.media-amazon.com/images/I/51waks01PAL.jpg"
    )
    expect(book.errors.messages.keys.include?(:title))
    expect(book).to_not be_valid
  end

  it "is not valid without a description" do
    book = Book.new(
      author: @author, 
      title: "The Lord of the Rings: The Fellowship of the Ring", 
      description: nil,
      image: "https://m.media-amazon.com/images/I/51waks01PAL.jpg"
    )
    expect(book.errors.messages.keys.include?(:description))
    expect(book).to_not be_valid
  end

  it "is not valid without a image" do
    book = Book.new(
      author: @author, 
      title: "The Lord of the Rings: The Fellowship of the Ring", 
      description: "The dark, fearsome Ringwraiths ....",
      image: nil
    )
    expect(book.errors.messages.keys.include?(:image))
    expect(book).to_not be_valid
  end

end
