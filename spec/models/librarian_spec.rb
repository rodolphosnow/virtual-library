require 'rails_helper'

RSpec.describe Librarian, type: :model do
  it "is valid with valid attributes" do
    librarian = Librarian.new(name: "Lucas", email: "lucasteste@gmail.com", password: "teste123")
    expect(librarian).to be_valid
  end

  it "is not valid without a name" do
    librarian = Librarian.new(name: nil, email: "lucasteste@gmail.com", password: "teste123")
    librarian.valid?
    expect(librarian.errors.messages.keys.include?(:name))
    expect(librarian).to_not be_valid
  end

  it "is not valid without a password" do
    librarian = Librarian.new(name: "Lucas", email: "lucasteste@gmail.com", password: nil)
    librarian.valid?
    expect(librarian.errors.messages.keys.include?(:password))
    expect(librarian).to_not be_valid
  end

  it "is not valid without a email" do
    librarian = Librarian.new(name: "Lucas", email: nil, password: "teste123")
    librarian.valid?
    expect(librarian.errors.messages.keys.include?(:email))
    expect(librarian).to_not be_valid
  end

  it "is not valid with name length bellow 2" do
    librarian = Librarian.new(name: "J", email: "lucasteste@gmail.com", password: "teste123")
    librarian.valid?
    expect(librarian.errors.messages.keys.include?(:name))
    expect(librarian).to_not be_valid
  end

  it "is not valid with name length above 60" do
    librarian = Librarian.new(name: "abcdefghijklmnopkrstuvxzabcdefghijklmnopkrstuvxz1234567890123", email: "lucasteste@gmail.com", password: "teste123")
    librarian.valid?
    expect(librarian.errors.messages.keys.include?(:name))
    expect(librarian).to_not be_valid
  end
end
