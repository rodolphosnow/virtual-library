require 'rails_helper'

RSpec.describe FavoriteBook, type: :model do

  describe "Associations" do
    it { should belong_to(:user).without_validating_presence }
  end

  before(:each) do
    @author = Author.create(name: "J. R. R. Tolkien")
    @book = Book.create(
      author: @author, 
      title: "The Lord of the Rings: The Fellowship of the Ring", 
      description: "The dark, fearsome Ringwraiths ....",
      image: "https://m.media-amazon.com/images/I/51waks01PAL.jpg"
    )
    @user = User.create(name: "John", email: "johnteste@gmail.com", password: "teste123")
  end

  it "is valid with valid attributes" do
    favorite_book = FavoriteBook.new(book: @book, user: @user)
    expect(favorite_book).to be_valid
  end

  it "is invalid without book" do
    favorite_book = FavoriteBook.new(book: nil, user: @user)
    expect(favorite_book).to_not be_valid
  end

  it "is invalid without user " do
    favorite_book = FavoriteBook.new(book: @book, user: nil)
    expect(favorite_book).to_not be_valid
  end

  it "is invalid for already existant book user combination" do
    favorite_book = FavoriteBook.create(book: @book, user: @user)
    favorite_book_repeated = FavoriteBook.new(book: @book, user: @user)
    expect(favorite_book_repeated).to_not be_valid
  end

end
